var S = require('string');
var URL = require('url');
var keyword = '';

exports.scraper = {
    name: 'bingimg',
    useTor:true,
    url: function () {
        return "http://www.bing.com/images/async?view=detailV2&q="+ encodeURIComponent(keyword) +"&selectedIndex=0&ADLT=Strict&MKT=en-US";
    },
	useBingJson:true,
    setup:function(req){
        keyword=req.query.keyword
    },
    rows: function (s) {
        return s.web;
    },
    fields: {
        title: function(rec){
            var title = rec.title;
            if(!title) return "";
            return title.replace(/\+/g , " ");
        },
        body: function(rec){
            var str = rec.title;
            if(!str) str = ""; 
            return str.replace(/\+/g , " ");
        },
        thumb: function(rec) {
            var img_string = rec.thUrl;
            if(!img_string) return "";  
            return img_string+"&h=250&w=250";
        },
        surl: function(rec){
            var m = rec.domain;
            if(!m) m = "";
            return m;
        },
		furl: function(rec){
            var m = rec.imgUrl;
            if(!m) m = "";
            return m;
        },
		rurl: function(rec){
            var m = rec.pageUrl;
            if(!m) m = "";
            return m;
        },
		filetype: function(rec){
            var m = rec.fmt;
            if(!m) m = "";
            return m;
        },
		size: function(rec){
            return "";
        },
		h: function(rec){
            var m = rec.height;
            if(!m) m = "";
            return m;
        },
		w: function(rec){
            var m = rec.width;
            if(!m) m = "";
            return m;
        }
    }
};