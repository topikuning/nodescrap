var S = require('string');
var URL = require('url');
var keyword = '';

exports.scraper = {
    name: 'bing',
    url: function () {
        return "http://www.amazon.com/s/ref=nb_sb_noss_1?url=search-alias%3Daps&field-keywords="+ encodeURIComponent(keyword);
    },
    setup:function(req){
        keyword=req.query.keyword
    },
    rows: function ($) {
        return $('li.s-result-item:not(#result_0)');
    },
    fields: {
        title: function($){
            return S($.find('h2.s-access-title').text()).s;
        },
        asin:function($){
            return $.attr('data-asin');
        },
        body: function($){
            return S($.find('h2.s-access-title').text()).s;
        },
        thumb_url: function ($) {
            var img_string = $.find('img.s-access-image').attr('src');
            return img_string;
        },
        image: function ($){
            var img_string = $.find('img.s-access-image').attr('src');
            return {
                surl:'amazon.com',
                imgurl:img_string
            };
        }
    }
};