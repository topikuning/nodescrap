var S = require('string');
var URL = require('url');
var keyword = '';

exports.scraper = {
    name: 'yandex',
    //useCasper:true,
    url: function () {
        return "https://www.yandex.com/search/?text="+ encodeURIComponent(keyword)+"&mime=pdf&lr=21756";
    },
    setup:function(req){
        keyword=req.query.keyword
    },
    rows: function ($) {
        return $('div.serp-item.serp-item_plain_yes.i-bem');
    },
    fields: {
        title: function($){
            return S($.find('h2.serp-item__title').text()).s;
        },
        abstract: function($){
            return S($.find('div.serp-item__text').text()).s;
        },
        pdf_file: function($){
            return $.find('h2.serp-item__title').find('a').attr('href');
        }
   }

};
