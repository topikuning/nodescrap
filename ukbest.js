var S = require('string');
var URL = require('url');
var s_url = 'http://www.amazon.co.uk/gp/bestsellers/officeproduct/430586031/ref=s9_ri_bw_clnk?pf_rd_m=A3P5ROKL5A1OLE&pf_rd_s=merchandised-search-11&pf_rd_r=0XSC7W0G872H8KEN965D&pf_rd_t=101&pf_rd_p=526132907&pf_rd_i=428653031';

exports.scraper = {
    useTor:true,
    name: 'ukbest',
    url: function () {
        return s_url;
    },
    setup:function(req){
        //s_url=req.query.s_url;
    },
    rows: function ($) {
        return $('div.zg_itemWrapper');
    },
    fields: {
        title: function($){
            var title = $.find('div.zg_title a').text();
            return title;
        },
        url: function($){
            var url = $.find('div.zg_title a').attr('href');
            url =  url.replace(/\n/g, "");
            return url;
        },
        image:function($){
            var img_url = $.find('div.zg_itemImageImmersion img').attr('src'); 
            return img_url; 
        },
        asin:function($){
            var asin = $.find('.asinReviewsSummary').attr('name'); 
            return asin;
        },
        price:function($){
            return $.find('strong.price').text().substring(1);
        }
    }
};