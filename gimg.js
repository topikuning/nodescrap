var S = require('string');
var URL = require('url');
var keyword = '';
var size = 'large';

exports.scraper = {
    name: 'gimg',
    url: function (index) {
        return "https://www.googleapis.com/customsearch/v1?q="+ encodeURIComponent(keyword) +"&cx=partner-pub-9133994767086422:7425317591&key=AIzaSyBi1F711Nm5p8SW9CEIZlJ5CHZqt0KKjx0&searchType=image&filter=1&safe=high&imgSize="+size;
    },
    useGoogleJson:true,
    setup:function(req){
        if(req.query.keyword) keyword=req.query.keyword;
        //icon, small, medium, large, xlarge, xxlarge, and huge
		if(req.query.size) size=req.query.size;
    },
    rows: function (s) {
        return s.items;
    },
    fields: {
		title: function(rec){
			var title = rec.title.replace(/\.\.\./g,"");
			var snippet = rec.snippet.replace(/\.\.\./g,"");
			title = title.trim();
			snippet = snippet.trim();
			var judul = title.concat(" at ",snippet);
			return S(judul).s;
		},
		body: function(rec){
			var title = rec.title.replace(/\.\.\./g,"");
			title = title.trim();
			return S(title).s;
		},
		thumb: function(rec){
			return rec.image.thumbnailLink;
		},
		surl: function(rec){
			return rec.displayLink;
		},
		furl: function(rec){
			return rec.link;
		},
		rurl: function(rec){
			return rec.image.contextLink;
		},
		filetype: function(rec){
			var mime = rec.mime;
			var tipe = mime.split("/");
			return tipe[1];
		},
		size: function(rec){
			var ukuran = Math.round(rec.image.byteSize / 1000);
			return ukuran+"kB";
		},
		w: function(rec){
			return rec.image.width;
		},
		h: function(rec){
			return rec.image.height;
		}
   }
};