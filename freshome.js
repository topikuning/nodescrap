var urle = '';

exports.scraper = {
    name: 'freshome',
    useTor:true,
    url: function () {
        return urle;
    },
    setup:function(req){
        urle=req.query.urle
    },
    rows: function ($) {
        return $('article.news-item');
    },
    fields: {
        user: function($){
			var user = $.find('div.author-media-body').find('span.hidden-xs').find('span.author-info').find('a').text();
			if(!user) return "";
            return user;
        },
		gravatar: function($){
			var gravatar = $.find('div.author-media-body').find('span.author-info').find('img').attr('src');
			if(!gravatar) return "";
			return gravatar;
        },
		title: function($){
			var title = $.find('h2.news-item-title.visible-xs').find('a').text();
			if(!title) return "";
            return title;
        },
		tags: function($){
			var tag = $.find('div.author-media-body').find('span.hidden-xs').find('a').text();
			tags=tag.replace(/([a-z])([A-Z])/g, '$1|$2')
			if(!tags) return "";
            return tags;
        },
		excerpt: function($){
			var excerpt = $.find('div.news-item-content').find('p').text().slice(0, -3);
			if(!excerpt) return "";
            return excerpt.trim();
        },
        thumb_url: function ($) {
			var src = $.find('div.news-item-img').find('a').find('img').attr('src');
			if(!src) return "";
			var src = src.replace("http://", "");
			var src = src.replace("https://", "");
			var wp = Math.floor((Math.random() * 4) + 1) - 1;
			var lnk = 'http://i'+wp+'.wp.com/'+src+'?resize=252,252';
            return lnk;
        },
		image: function ($) {
			var src = $.find('div.news-item-img').find('a').find('img').attr('src');
			if(!src) return "";
            return src;
        },
        url: function ($){
            var lnk = $.find('div.news-item-img').find('a').attr('href');
            if(!lnk) return "";
            return lnk;
        }
    }
};