var S = require('string');
var URL = require('url');
var keyword = '';

exports.scraper = {
    name: 'gimage2',
    //useTor:true,
    url: function () {
		return "https://www.google.nl/search?hl=en&site=imghp&source=lnms&tbm=isch&sa=X&ved=0ahUKEwihoem3os3RAhWFJMAKHaBqD8cQ_AUIBigB&tbs=itp:photo,isz:lt,islt:vga&q="+ encodeURIComponent(keyword);
    },
    setup:function(req){
        keyword=req.query.keyword
    },
    rows: function ($) {
        return $('div.rg_di.rg_bx.rg_el.ivg-i');
    },
    fields: {
        title: function($){
            var title = $.find('div.rg_meta').text();
            if(!title) return "";
			var dt = JSON.parse(title);
            return dt.pt.replace(/\\n/, "").replace(/&#(\d+);/g, function(match, dec) {
				return String.fromCharCode(dec);
			});
        },
        body: function($){
            var str = $.find('div.rg_meta').text();
            if(!str) return "";
			var ds = JSON.parse(str);
            return ds.s.replace(/\\n/, "").replace(/&#(\d+);/g, function(match, dec) {
				return String.fromCharCode(dec);
			});
        },
        thumb: function ($) {
            var img_string = $.find('div.rg_meta').text();
            if(!img_string) return "";  
            var dth = JSON.parse(img_string);
			var preview = dth.ou;
			if(!preview) return "";
			preview = preview.replace("http://","");
			preview = preview.replace("https://","");
			iwp = Math.floor(Math.random() * 4) + 0;
			return "http://i"+iwp+".wp.com/"+preview+"?resize=250,250";
        },
        surl: function ($){
            var img = $.find('div.rg_meta').text();
			var dim = JSON.parse(img);
			return dim.isu;
        },
		furl: function ($){
            var img = $.find('div.rg_meta').text();
			var dim = JSON.parse(img);
			return dim.ou;
        },
		rurl: function ($){
            var img = $.find('div.rg_meta').text();
			var dim = JSON.parse(img);
			return dim.ru;
        },
		filetype: function ($){
            var img = $.find('div.rg_meta').text();
			var dim = JSON.parse(img);
			return dim.ity;
        },
		size: function ($){
            var img = $.find('div.rg_meta').text();
			var dim = JSON.parse(img);
			return "";
        },
		h: function ($){
            var img = $.find('div.rg_meta').text();
			var dim = JSON.parse(img);
			return dim.oh;
        },
		w: function ($){
            var img = $.find('div.rg_meta').text();
			var dim = JSON.parse(img);
			return dim.ow;
        }
    }
};
