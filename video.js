var S = require('string');
var URL = require('url');
var keyword = '';

exports.scraper = {
    name: 'video',
    useTor:true,
    url: function () {
        return "http://www.bing.com/videos/search?q="+ encodeURIComponent(keyword)+"&qft=+filterui:msite-youtube.com&ADLT=Strict&MKT=en-US";
    },
    setup:function(req){
        keyword=req.query.keyword
    },
    rows: function ($) {
        return $('div.dg_u');
    },
    fields: {
        title: function($){
            var str = S($.find('a').attr('vrhm')).s;
                        var obj = JSON.parse(str);
                        var title=obj.t;
            if(!title) return "";
            return title;
        },
        body: function($){
            var str = S($.find('a').attr('vrhm')).s;
                        var obj = JSON.parse(str);
                        var body=obj.de;
            if(!body) body = "";
            return body;
        },
        thumb_url: function ($) {
                        var str = S($.find('a').attr('vrhm')).s;
                        var obj = JSON.parse(str);
                        var src=obj.p;
                        var thumb_url = "http://img.youtube.com/vi/" + src.substr(src.indexOf("=") + 1) + "/0.jpg";
            if(!thumb_url) thumb_url = "";
            return thumb_url;
        },
        image: function ($){
            var str = S($.find('a').attr('vrhm')).s;
                        var obj = JSON.parse(str);
                        var src=obj.p;
                        var video = "http://www.youtube.com/embed/" + src.substr(src.indexOf("=") + 1);
            if(!video) video = "";
            return {
                surl:"",
                imgurl:video
            };
        }
    }
};