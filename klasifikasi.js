var S = require('string');
var URL = require('url');
var img = '';

exports.scraper = {
    name: 'klasifikasi',
    useTor:true,
    url: function () {
        return "http://demo.caffe.berkeleyvision.org/classify_url?imageurl="+ encodeURIComponent(img);
    },
    setup:function(req){
        img=req.query.img
    },
    rows: function ($) {
        return $('li.list-group-item');
    },
    fields: {
        title: function($){
            var title = S($.find('a').text()).s;
            if(!title) return "";
            return title;
        }
    }
};