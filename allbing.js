var S = require('string');
var URL = require('url');
var keyword = '';

exports.scraper = {
    name: 'bigbing',
    useTor:true,
    url: function () {
        return "http://www.bing.com/images/search?q="+ encodeURIComponent(keyword)+"&qft=+filterui:imagesize-custom_800_600+filterui:photo-photo&ADLT=Strict&MKT=en-US";
    },
    setup:function(req){
        keyword=req.query.keyword
    },
    rows: function ($) {
        return $('html');
    },
    fields: {
		related: function ($,$$) {
            var keyword=[]
            $.find('div.sug').each(function(index,element){
                var o={}
				var kwne = $$(element).find('a').attr('title');
				if(kwne){
					o.keyword=kwne.replace("Search for:","").trim();
					keyword.push(o)
				}
            });
            if(!keyword) return "";
            return keyword;
        },
		images: function ($,$$) {
            var gambar=[]
			$.find('div.dg_u').each(function(index,element){
                var o={}
                
				var title = S($$(element).find('a').attr('t1')).s;
				if(!title) title="";
				o.title=title
				
				var body =$$(element).find('a').attr('t0');
				if(!body) body = $$(element).find('a').attr('t1');
				if(!body) body = "";
				o.body=body
				
				var img_string = $$(element).find('a').find('img').attr('src2');
				if(!img_string) {
					thumb = "";  
				} else {
					var arr = img_string.split('&');
					var tmp = []; 
					for (var i = 0; i < arr.length; i++) {
						var item = arr[i]; 
						if(item.indexOf('h=') < 0 ){
							if(item.indexOf('w=') > -1){
								item = 'w=250';
							}
							tmp.push(item);
						}

					};
					thumb = tmp.join('&');
				}
				o.thumb = thumb;
				
				var m = $$(element).find('a').attr('m');
				var obj = eval('(' + m +')');
				o.surl=URL.parse(obj.surl).hostname,
				o.furl=obj.imgurl
				
				o.rurl = obj.surl;
				
				var atribut = $$(element).find('a').attr('t2');
				atrib = atribut.split("·");
				ukuran = atrib[0].split("x");
				o.filetype=atrib[2].trim();
				o.size=atrib[1].trim();
				o.h=ukuran[1].trim();
                o.w=ukuran[0].trim();
				
                gambar.push(o)
            });
			if(!gambar) return "";
			return gambar;
        }
    }
};