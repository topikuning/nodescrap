var S = require('string');
var URL = require('url');
var urle = '';

exports.scraper = {
    name: 'textfetch',
    useTor:true,
    url: function () {
        return 'http://pdf.fivefilters.org/makepdf.php?v=2.5&url='+encodeURIComponent(urle)+'&mode=single-story&output=html&template=A4&title=&order=desc&api_key=&sub=';
    },
    setup:function(req){
        urle=req.query.urle
    },
    rows: function ($) {
        return $('html');
    },
    fields: {
        title:function($){
            return $.find('article.story').find('h1.title').text()
        },
		body:function($){
            var full = $.find('article.story').html()
			var newstring = full.replace(/<h1[^>]*>(.*)<\/h1>/, "");
			return newstring;
        }
    }
};