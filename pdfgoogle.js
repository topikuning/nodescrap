var S = require('string');
var URL = require('url');
var keyword = '';

exports.scraper = {
    name: 'pdfgoogle',
    url: function () {
        return "https://www.googleapis.com/customsearch/v1element?key=AIzaSyCVAXiUzRYsML1Pv6RwSG1gunmMikTzQqY&start=10&cx=partner-pub-9634067433254658:9653363797&q="+ encodeURIComponent(keyword) +"+filetype%3Apdf&alt=json&rsz=filtered_cse&num=10&hl=en&source=gcsc&gss=.com&sig=cb6ef4de1f03dde8c26c6d526f8a1f35";
    },
    useGoogleJson:true,
    setup:function(req){
        keyword=req.query.keyword
    },
    rows: function (s) {
        return s.results;
    },
    fields: {
        title: function(rec){
            var title =  S(rec.titleNoFormatting).s;
            if(!title) return ""; 
            return title;
        },
        abstract: function(rec){
            return S(rec.contentNoFormatting).s;
        },
        pdf_file: function(rec){
            return rec.unescapedUrl;
        }
   }

};