var S = require('string');
var URL = require('url');
var keyword = '';

exports.scraper = {
    name: 'bookgoogle',
    url: function () {
        return "http://www.google.com/books/feeds/volumes?q="+ encodeURIComponent(keyword);
    },
    useGoogleJson:true,
    isXml:true,
    useTor:true,
    setup:function(req){
        keyword=req.query.keyword
    },
    rows: function (s) {
        return s.feed.entry;
    },
    fields: {
        title: function(rec){
            return rec.title['$t'];
        },
        abstract:function(rec){
            return rec['dc:description'];
        },

        categories:function(rec){
            return rec['dc:subject'];
        },
        published:function(rec){
            return rec['dc:date'];
        }, 
        rating:function(rec){
            return rec['gd:rating'];
        }, 
        page_count:function(rec){
            var x =  rec['dc:format'][0];
            x = parseInt(x); 
            return (x)?x:'0';
        }, 
        authors:function(rec){
            return rec['dc:creator'];
        },

        thumbnail:function(rec){
            var img_url = ""; 
            var link = rec.link;
            for (var i = 0; i < link.length; i++) {
                var type = link[i].type; 
                if(type.indexOf('image') > -1){
                    img_url = link[i].href;
                }
            };

            if(img_url){
                img_url = img_url.replace('zoom=5', 'zoom=1');
            }

            return img_url;
        }
   }

};