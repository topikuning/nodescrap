var S = require('string');
var URL = require('url');
var keyword = '';

exports.scraper = {
    name: 'bsuggest',
    useTor:true,
    url: function () {
        return "http://www.bing.com/images/search?q="+ encodeURIComponent(keyword)+"&ADLT=Strict&MKT=en-US";
    },
    setup:function(req){
        keyword=req.query.keyword
    },
    rows: function ($) {
        return $('div.sug');
    },
    fields: {
        result: function($){
            var title = $.find('a').attr('title');
            if(!title) return false;
            return title.replace("Search for:","").trim();
        }
    }
};