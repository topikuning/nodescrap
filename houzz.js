var urle = '';

exports.scraper = {
    name: 'houzz',
    useTor:true,
    url: function () {
        return urle;
    },
    setup:function(req){
        urle=req.query.urle
    },
    rows: function ($) {
        return $('div.whiteCard');
    },
    fields: {
        title: function($){
			var user = $.find('span.hz-username.hzHouzzer.hzHCUserName').text();
			var title = $.find('a.photo-title').text();
            if(!title) title = $.find('a.product-title').text();
			if(!title) return "";
			var gab = title;
			if(user) gab = title.concat(" by ",user);
            return gab;
        },
        thumb_url: function ($) {
			var src = $.find('img.hide-context.space.reloadable').attr('src');
			thumb = "";
			if(src) thumb=src.replace (/-w.*-b0-p0/, "-w250-h250-b0-p0");
            return thumb;
        },
        url: function ($){
            var lnk = $.find('a.photo-title').attr('href');
            if(!lnk) lnk = $.find('a.product-title').attr('href');
            return lnk;
        }
    }
};