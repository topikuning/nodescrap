var S = require('string');
var URL = require('url');
var keyword = '';

exports.scraper = {
    name: 'wordbing',
    url: function () {
        return "http://www.bing.com/search?q="+ encodeURIComponent(keyword)+"+filetype%3Adoc&go=&qs=bs";
    },
    setup:function(req){
        keyword=req.query.keyword
    },
    rows: function ($) {
        return $('li.b_algo');
    },
    fields: {
        title: function($){
            return S($.find('h2').text()).s;
        },
        abstract: function($){
            return S($.find('p').text()).s;
        },
        pdf_file: function($){
            return $.find('h2').find('a').attr('href');
        }
   }

};