var S = require('string');
var URL = require('url');
var keyword = '';

exports.scraper = {
    name: 'bing',
    useTor:true,
    url: function () {
        return "http://www.bing.com/images/search?scope=images&count=1&q="+ encodeURIComponent(keyword)+"&qft=+filterui:photo-photo+filterui:imagesize-wallpaper&ADLT=Strict&MKT=en-US";
    },
    setup:function(req){
        keyword=req.query.keyword
    },
    rows: function ($) {
        return $('div.dg_u');
    },
    fields: {
        title: function($){
            var title = S($.find('a').attr('t1')).s;
            if(!title) return "";
            return title;
        },
        body: function($){
            var str =$.find('a').attr('t0');
            if(!str) str = $.find('a').attr('t1'); 
            return str;
        },
        thumb_url: function ($) {
            var img_string = $.find('a').find('img').attr('src2');
            if(!img_string) return "";  
            var arr = img_string.split('&');
            var tmp = []; 
            for (var i = 0; i < arr.length; i++) {
                var item = arr[i]; 
                if(item.indexOf('h=') < 0 ){
                    if(item.indexOf('w=') > -1){
                        item = 'w=250';
                    }
                    tmp.push(item);
                }

            };
            return tmp.join('&');
            //return S($.find('.link.linkWithHash').text()).trim().s;
        },
        image: function ($){
            var m = $.find('a').attr('m');
            var obj = eval('(' + m +')');
            return {
                surl:URL.parse(obj.surl).hostname,
                imgurl:obj.imgurl
            };
        }
    }
};