var S = require('string');
var URL = require('url');
var keyword = '';

exports.scraper = {
    name: 'related',
    //useTor:true,
    url: function () {
        return "https://www.google.com/search?q="+ encodeURIComponent(keyword)+"&source=lnt&gws_rd=ssl";
    },
    setup:function(req){
        keyword=req.query.keyword
    },
    rows: function ($) {
        return $('p._e4b');
    },
    fields: {
        result: function($){
            var title = $.find('a').text();
            if(!title) return "";
            return title;
        }
    }
};