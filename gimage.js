var S = require('string');
var URL = require('url');
var keyword = '';

exports.scraper = {
    name: 'gimage',
    //useTor:true,
    url: function () {
        return "https://www.google.com/search?q="+ encodeURIComponent(keyword)+"&tbs=isz:lt,islt:svga,itp:photo&tbm=isch&source=lnt&safe=active";
    },
    setup:function(req){
        keyword=req.query.keyword
    },
    rows: function ($) {
        return $('div.rg_di.rg_bx.rg_el.ivg-i');
    },
    fields: {
        title: function($){
            var title = $.find('div.rg_meta').text();
            if(!title) return "";
			var dt = JSON.parse(title);
            return dt.pt;
        },
        body: function($){
            var str = $.find('div.rg_meta').text();
            if(!str) return "";
			var ds = JSON.parse(str);
            return ds.s;
        },
        thumb_url: function ($) {
            var img_string = $.find('div.rg_meta').text();
            if(!img_string) return "";  
            var dth = JSON.parse(img_string);
			preview = dth.ou;
			preview = preview.replace("http://","");
			preview = preview.replace("https://","");
			iwp = Math.floor(Math.random() * 4) + 0;
			return "http://i"+iwp+".wp.com/"+preview+"?resize=250%2C250";
        },
        image: function ($){
            var img = $.find('div.rg_meta').text();
			var dim = JSON.parse(img);
            return {
                surl:dim.isu,
                imgurl:dim.ou
            };
        }
    }
};