var S = require('string');
var URL = require('url');
var keyword = '';

exports.scraper = {
    name: 'cbing',
    url: function () {
        return "http://s.cn.bing.net/images/search?q="+ encodeURIComponent(keyword)+"&qft=+filterui%3Aimagesize-wallpaper";
    },
    setup:function(req){
        keyword=req.query.keyword
    },
    rows: function ($) {
        return $('div.dg_u');
    },
    fields: {
        title: function($){
            return S($.find('a').attr('t1')).s;
        },
        body: function($){
            var str =$.find('a').attr('t0');
            if(!str) str = $.find('a').attr('t1'); 
            return str;
        },
        thumb_url: function ($) {
            var img_string = $.find('a').find('img').attr('src2');
            var arr = img_string.split('&');
            var tmp = []; 
            for (var i = 0; i < arr.length; i++) {
                var item = arr[i]; 
                if(item.indexOf('h=') < 0 ){
                    if(item.indexOf('w=') > -1){
                        item = 'w=250';
                    }
                    tmp.push(item);
                }

            };
            return tmp.join('&');
            //return S($.find('.link.linkWithHash').text()).trim().s;
        },
        image: function ($){
            var m = $.find('a').attr('m');
            var obj = eval('(' + m +')');
            return {
                surl:URL.parse(obj.surl).hostname,
                imgurl:obj.imgurl
            };
        }
    }
};