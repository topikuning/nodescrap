var S = require('string');
var URL = require('url');
var keyword = '';

exports.scraper = {
    name: 'pinterest',
    //useTors:true,
    url: function () {
        return "https://www.pinterest.com/search/pins/?q="+ encodeURIComponent(keyword);
    },
    setup:function(req){
        keyword=req.query.keyword
    },
    rows: function ($) {
        return $('html');
    },
    fields: {
		related: function ($,$$) {
            return "";
        },
		images: function ($,$$) {
            var gambar=[]
			$.find('div.item').each(function(index,element){
                var o={}
                
				var title = $$(element).find('h3.richPinGridTitle').text();
				if(!title) title=S($$(element).find('a.pinImageWrapper').attr('title')).s;
				if(!title) title="";
				o.title=title.trim();
				
				var body =S($$(element).find('a.pinImageWrapper').attr('title')).s;
				if(!body) body = "";
				o.body=body.trim();
				
				var thumb = $$(element).find('img.pinImg.fullBleed').attr('src');
				if(!thumb) thumb = $$(element).find('img.pinImg').attr('src');
				if(!thumb) thumb = "";
				o.thumb = thumb;
				
				var creditName = $$(element).find('div.creditName').text();
				var creditTitle = $$(element).find('div.creditTitle').text();
				o.surl=creditName.concat(" at " + creditTitle.trim());
				o.furl=thumb.replace("/236x/", "/564x/");
				
				var urlasli = $$(element).find('a.Button.Module.NavigateButton.borderless.hasIcon.hasText.pinNavLink.navLinkOverlay').attr('href');
				if(!urlasli) urlasli = "";
				o.rurl = urlasli;
				o.filetype="jpg";
                gambar.push(o)
            });
			if(!gambar) return "";
			return gambar;
        }
    }
};