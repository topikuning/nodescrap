var S = require('string');
var URL = require('url');
var keyword = '';

exports.scraper = {
    name: 'gsuggest',
    //useTor:true,
    url: function () {
        return 'http://suggestqueries.google.com/complete/search?output=firefox&client=firefox&hl=en-US&gl=us&q='+ encodeURIComponent(keyword);
    },
    useGoogleJson:true,
    setup:function(req){
        keyword=req.query.keyword;
    },
    rows: function (s) {
        return s[1];
    },
    fields: {
        result: function(rec){
            return rec;
        }
   }

};